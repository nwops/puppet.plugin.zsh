# Git version checking
autoload -Uz is-at-least
git_version="${${(As: :)$(git version 2>/dev/null)}[3]}"

#
# Functions
#


#
# Aliases
# (sorted alphabetically)

# PDK aliases
alias pbe='pdk bundle exec'
alias pbr='pbe bundle exec rake'
alias pcon='pdk console'
alias pconx="pdk console --facterdb-filter='operatingsystem=Darwin'"
alias pcond="pdk console --facterdb-filter='operatingsystem=debian'"
alias pconw="pdk console --facterdb-filter='operatingsystem=windows'"
alias pconr="pdk console --facterdb-filter='operatingsystem=RedHat and operatingsystemmajrelease=7'"
alias pnm='pdk new module'
alias pnm!='pnm  --skip-interview'
alias ptu='pdk test unit'
alias pva='pdk validate -a'
alias pv='pdk validate'
alias ppry='pbe pry'


# Bolt aliases


# Puppet aliases


# puppet client tools


# r10k 

