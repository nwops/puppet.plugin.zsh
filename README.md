# puppet.plugin.zsh

Puppet related Shell completions for zsh

## Setup
1. You first must install [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
2. Download or clone puppet zsh plugin and copy/move to the directory ~/.oh-my-zsh/plugins/
   
   `git clone https://gitlab.com/nwops/puppet.plugin.zsh.git ~/.oh-my-zsh/plugins/puppet`
3. Edit the the following lines in the file .zshrc

	```
	# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
	# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
	# Example format: plugins=(rails git textmate ruby lighthouse)
	# Add wisely, as too many plugins slow down shell startup.
    # Add the puppet plugin to your list of plugins
	plugins=(... puppet)

	```
	
4. Run `source ~/.zshrc`

## Common aliases

| Alias                   | Command                        | Description                                                                                                 |
| ----------------------- | ------------------------------ | ----------------------------------------------------------------------------------------------------------- |
| pbe                     | pdk bundle exec                | Run a bundle command                                                                          |
| pbr                     | pdk bundle exec rake           | Run a rake task with pdk                                                                 |
| pv                      | pdk valiate                    | Run the pdk validate command                                                                      |
| pva                     | pdk valiate -a                 | Run the pdk valiate tests with autocorrect                                    |
| pnm                     | pdk new module                 | Create a new pdk module                                                |
| pnm!                    | pdk new module --skip-interview | Creat a new pdk module quickly                                                          |
| pcon                    | pdk console                    | Run the pdk console |
| pconw                    | pdk console                    | Run the pdk console with window facts from facterdb  |
| pconx                    | pdk console                    | Run the pdk console with os x facts from facterdb  |
| pconr                    | pdk console                    | Run the pdk console with redhat facts from facterdb  |
| pcond                    | pdk console                    | Run the pdk console with debian facts from facterdb |
| ptu                    | pdk test unit                  | Run unit tests |
